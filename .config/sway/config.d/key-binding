########################################################
####					Variables					####
########################################################

#######################
#### App Variables ####
#######################

	set $term		 alacritty
	set $filemanager nautilus
	set $browser1	 firefox
	set $browser2	 chromium -enable-features=UseOzonePlatform -ozone-platform=wayland
	set $email		 evolution
	set $editor		 subl3
	set $pkgmanager	 pamac-manager
	set $music		 xdg-open https://www.youtube.com/channel/UC-9-kyTW8ZkZNDHQJ6FgpwQ
	set $video1		 stremio
	set $video2		 DRI_PRIME=1 kodi
	set $bt			 transmission-gtk -m
	set $vm			 virt-manager


#################################
#### App launchers Variables ####
#################################
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.

	set $appmenu wofi --show drun --prompt "Application Launcher" | xargs swaymsg exec --
	set $menu wofi --show run --exec-search --prompt "Run Command" | xargs swaymsg exec --
	set $websearch $HOME/.config/wofi/wofi-websearch.sh
	set $search ulauncher-toggle


##########################
#### System Variables ####
##########################

	set $lockscreen swaylock -f -c 000000BF
	set $shutdown nwgbar


############################################################
####					Keybindings 					####
############################################################

######################
#### Non Variable ####
######################

	bindsym $mod+t sticky toggle
	bindsym $mod+Tab workspace next

	# Kill focused window
	bindsym $mod+q kill

	# Reload the configuration file
   	bindsym $mod+Shift+c reload


###################
#### Main Apps ####
###################

	bindsym $mod+Return exec $term
	bindsym $mod+Shift+Return exec $filemanager

######################
#### Apps FN Keys ####
######################

	bindsym $mod+f1 exec $browser1
	bindsym $mod+f2 exec $email
	bindsym $mod+f3 exec $editor
	bindsym $mod+f4 exec $video1
	bindsym $mod+f5 exec $vm
	bindsym $mod+f6 exec $bt

##############
#### Apps ####
##############

	bindsym $mod+p exec $pkgmanager


#######################
#### App Launchers ####
#######################

	bindsym $mod+Shift+d exec $search
	bindsym $mod+d exec $appmenu
	bindsym $mod+slash exec $websearch


##########################
#### Keyboard symbols ####
##########################

	bindsym XF86HomePage exec firefox
	bindsym XF86Mail exec geary
	bindsym XF86Search exec $websearch
	bindsym XF86Music exec $music
	
	bindsym XF86Back exec
	bindsym XF86Favorites exec $music
	bindsym XF86Calculator exec gnome-calculator


####################	
#### Media Keys ####
####################

	bindsym XF86AudioRaiseVolume exec pamixer -ui 2 && exec ~/.config/scripts/vol
	bindsym XF86AudioLowerVolume exec pamixer -ud 2 && exec ~/.config/scripts/vol
	bindsym XF86AudioMute exec pamixer --toggle-mute && ( pamixer --get-mute && echo 0 > $SWAYSOCK.wob ) || exec ~/.config/scripts/vol
	
	bindsym XF86AudioPlay	exec playerctl play-pause
	bindsym XF86AudioNext	exec playerctl next
	bindsym XF86AudioPrev	exec playerctl previous

	bindcode 360 exec ir-keytable -k KEY_ENTER


#################################
#### Brightness Key Bindings ####
#################################

	bindsym XF86MonBrightnessDown exec light -U 5 && light -G | cut -d'.' -f1 > $SWAYSOCK.wob
	bindsym XF86MonBrightnessUp exec light -A 5 && light -G | cut -d'.' -f1 > $SWAYSOCK.wob
    

#############################    
#### System Key Bindings ####
#############################

	bindsym XF86Sleep exec $shutdown
	bindsym $mod+Escape exec $lockscreen
    bindsym $mod+Shift+e exec $shutdown
