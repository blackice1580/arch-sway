# .bash_profile

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec ~/.config/scripts/sway-nvidia-run
fi

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 2 ]; then
  exec ~/.config/scripts/gnome-run
fi

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc
